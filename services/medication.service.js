
"use strict";
const { ServiceBroker } = require("moleculer");
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
const DbMixin = require("../mixins/db.mixin");
const MongoClient = require('mongodb').MongoClient;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
let mongoDb;

module.exports = {
    name: "medication",
    mixins: [DbMixin("medication")],
    settings: {
        fields: [
            "_id",
            "name",
            "time",
            "days"
        ],

        metrics: {
            // Don't add `ctx.params` to metrics payload. Default: false
            params: true,
            // Add `ctx.meta` to metrics payload. Default: true
            meta: true
        }
    },
    dependencies: [],
    hooks: {
    },
    actions: {
        medlist: {
            rest: {
                method: "GET",
                path: "/medlist"
            },

            async handler() {
                return await this.adapter.find();
            }
        },
        addMed: {
            rest: {
                method: "POST",
                path: "/addMed"
            },
            cache: true,
            metrics: {
                // Don't add `ctx.params` to metrics payload. Default: false
                params: true,
                // Add `ctx.meta` to metrics payload. Default: true
                meta: true
            },

            /** @param {Context} ctx  */
            async handler(ctx) {
                console.log( "testing ---------- " + JSON.stringify( ctx.params ) );
                return await this.adapter.insert( ctx.params );
            }
        },
        updateMed: {
            rest: {
                method: "PUT",
                path: "/updateMed",

            },

            /** @param {Context} ctx  */
            async handler(ctx) {
                //var value = JSON.parse( ctx );
                return await this.adapter.updateById( ctx.params._id, ctx.params );
            }
        },
        removeMed: {
            rest: {
                method: "DELETE",
                path: "/removeMed",
            },

            /** @param {Context} ctx  */
            async handler(ctx) {
                return await this.adapter.removeById( ctx.params._id );
            }
        }
    },
    events: {
    },
    methods: {

    },
    created() {
    },
    async started() {

    },
    async stopped() {

    }
};
