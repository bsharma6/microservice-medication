"use strict";

process.env.TEST=true;

const { ServiceBroker, Context } = require("moleculer");
const { ValidationError } = require("moleculer").Errors;
const TestService = require("../../services/medication.service");

describe("Test 'medication' service", () => {

	describe("Test actions", () => {
		const broker = new ServiceBroker({ logger: false });
		const service = broker.createService(TestService);
		service.seedDB = null; // Disable seeding

		beforeAll(() => broker.start());
		afterAll(() => broker.stop());

		const record = {
            _id: "123",
            name: "medicine name 1",
            days: "M:Tu:W",
            time: "11:30"
        };
		let newID;

		it("should contains the Medication list", async () => {
			const res = await broker.call("medication.medlist");
			expect(res).toEqual(expect.anything());
		});

		it("should add the new item", async () => {
			const res = await broker.call("medication.addMed", record);
			expect(res).toEqual({
                _id: "123",
                name: "medicine name 1",
                days: "M:Tu:W",
                time: "11:30"
            });
			newID = res._id;
		});

		it("should get the saved item", async () => {
			const res = await broker.call("medication.get", { id: newID });
			expect(res).toEqual({
                _id: "123",
                name: "medicine name 1",
                days: "M:Tu:W",
                time: "11:30"
            });

		});

		it("should update an item", async () => {
			const res = await broker.call("medication.update",  {
                    _id: "123",
                    name: "Advil",
                    days: "M:Tu:W",
                    time: "11:30"
                } );
			expect(res).toEqual({
                _id: "123",
                name: "Advil",
                days: "M:Tu:W",
                time: "11:30"
            });
		});

		it("should get the updated item", async () => {
			const res = await broker.call("medication.get", { id: newID });
			expect(res).toEqual({
                _id: "123",
                name: "Advil",
                days: "M:Tu:W",
                time: "11:30"
            });
		});
        it("should remove the updated item", async () => {
            const res = await broker.call("medication.remove", { id: newID });
            expect(res).toEqual({
                _id: "123",
                name: "Advil",
                days: "M:Tu:W",
                time: "11:30"
            });
        });

		});

});

