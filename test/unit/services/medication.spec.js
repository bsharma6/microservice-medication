"use strict";

process.env.TEST=true;

const { ServiceBroker, Context } = require("moleculer");
const { ValidationError } = require("moleculer").Errors;
const TestService = require("../../../services/medication.service");
const {MongoClient} = require('mongodb');
describe("Test 'medication' service", () => {

    describe("Test actions", () => {
        const broker = new ServiceBroker({ logger: false });
        const service = broker.createService(TestService);

        jest.spyOn(service.adapter, "updateById");
        jest.spyOn(service.adapter, "find");
        jest.spyOn(service, "transformDocuments");
        jest.spyOn(service, "entityChanged");

        beforeAll(() => broker.start());
        afterAll(() => broker.stop());

        const record = {
            _id: "123",
            name: "medicine name 1",
            days: "M:Tu:W",
            time: "11:30"
        };

        describe("Test GET all medicine list", () => {

            it("should call the adapter updateById method & transform result", async () => {
                service.adapter.find.mockImplementation(async () => record);
                const res = await broker.call("medication.medlist" );
                expect(res).toEqual({
                    _id: "123",
                    name: "medicine name 1",
                    days: "M:Tu:W",
                    time: "11:30"
                });
            });

        });
    });

});

